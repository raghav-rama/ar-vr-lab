using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class NumberTextIncreaser : MonoBehaviour {
    public TextMeshProUGUI numberText;
    int counter;
    public void ButtonPressed() {
        counter++;
        numberText.text = counter + "";
    }
    void Start() {}
    void Update() {}
}
